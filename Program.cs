﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace array
{
    class Program
    {
        static void Main(string[] args)
        {

            // 1. un programa que pida al usuario 4 numeros, los memorice ( utilizando un array), calcule su 
            // su media aritmetica y despues muestre en pantalla la media y los datos tecleados.
            /*
            int[] numero = new int[4];

            Console.WriteLine(" ingrese los numeros para calcular la media aritmetica \n");
            
            for (int i=0; i<numero.Length; i++)
            {
                try
                {
                    numero[i] = Convert.ToByte(Console.ReadLine());

                }catch(Exception e) when (e.GetType() != typeof(FormattableString)) // esto es para capturar cuando el usuario ingrese un valor de tipo string
                {
                    Console.WriteLine(" no has ingresado un valor numerico \n por tanto se guarda como cero 0" );
                    numero[i] = 0;
                }

            }

            int media = (numero.Sum())/4;

            Console.WriteLine($"\n la media aritmetica es {media} de los valores: ");

            // este for lo puse si quieren comprobar si se guardo el cero cuando se capturo la excepcion

            for (int i=0; i<numero.Length; i++)
            {
                Console.WriteLine(numero[i]);
            }

            */




            // 2. un programa que pida al usuario 5 numeros reales ( pista: necesitamos un array de "float" )
            // y luego los muestre en el orden contrario al que se introdujeron.

            /*
            float[] contrario = new float[5];
            

            Console.WriteLine(" ingrese los numero para mostrar");

            for(int i=0; i<5; i++)
            {
                contrario[i] = Convert.ToInt32(Console.ReadLine());
                
            }


            for (int j=4; j>=0; j--)
            {
                Console.WriteLine(contrario[j]);
            }

            */



            // 3 un programa que almacene en un array el numero de dia que tiene cada mes (ssupondremos que es un 
            // año no bisiesto) y pida al usuario el numero del mes y muestre en pantalla el numero de dia de ese mes
            /*
            
            int num;

            var meses= new[]
            {
                new {nombre= "mes no existe", dias=0 },
                new {nombre= "Enero", dias=31 },
                new {nombre= "Febrero", dias=28 },
                new {nombre= "Marzo", dias=31 },
                new {nombre= "Abril", dias=30 },
                new {nombre= "Mayo", dias=31 },
                new {nombre= "Junio", dias=30 },            // este un array de tipo anonimo
                new {nombre= "Junlio", dias=31 },
                new {nombre= "Agosto", dias=31 },
                new {nombre= "Semptiembre", dias=30 },
                new {nombre= "Octubre", dias=31 },
                new {nombre= "Noviembre", dias=30 },
                new {nombre= "Diciembre", dias=31 },

            };

            Console.WriteLine(" ingrese el numero que pertenece al Mes ");

            try
            {
                 num = Convert.ToByte(Console.ReadLine());

                Console.WriteLine(meses[num]);


            }
            catch (Exception e) when (e.GetType() != typeof(Range))
            {
                Console.WriteLine(" Mes no existe se  va a cerrar  el programa");
            }

            Console.Read(); 
            */



            // 4. un programa que pida al usuario 10 numeros y luego calcule y muestre cual es el mayor de todos 
            /*
            
            float [] numero = new float[10];
            float mayor=0;
            float iguales = 0;
            

            for (int i=0; i < numero.Length; i++)
            {
                try
                {
                    Console.WriteLine("ingrese los numeros");
                    numero[i] = Convert.ToInt32(Console.ReadLine());

                }catch (Exception e) when (e.GetType() != typeof(FormattableString))
                {
                    Console.WriteLine(" no has introducido un valor numerico");
                }

               
            }
            for (int i=0; i<numero.Length; i++)
            {
                if (numero[i] > mayor)
                {
                    mayor = numero[i];
                }

                
            }

            Console.WriteLine($" \n el numero mayor es: {mayor} ");
            */
            
            



            // 5. un programa que prepare espacio para un maximo de 100 nombres .  el el usuario debera introducir un 
            // nombre cada vez, hasta que se pulse intro momento sin teclear nada, moemento en el que dejara 
            // de pedir nombre y se mostrara en pantalla la lista de los nombres que se han introducidos

           /*
            * string[] nom = new string[100];

            for (int i=0; i<nom.Length; i++)
            {
                Console.WriteLine(" ingrese un nombre para guardar en el arreglo ");
                nom[i] = Console.ReadLine();

                if (nom[i] == "")
                {
                    break;
                }

            }
            Console.WriteLine("\n los nombres que ingresaste son: ");

            for(int i=0; i<nom.Length; i++)
            {
                Console.WriteLine(nom[i]);
            }
           */


        }
        
    } 





}

